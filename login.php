<?php
require_once 'banco.php';
?>

<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  </head>
  <body>
    <?php if ($_DB->login($_POST['username'], $_POST['password'])):?>
      <h1 class="main-title">Bem-vindo!</h1>
      <?php
        session_start();
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['password'] = $_POST['password'];
        header("Location: home.php");
      ?>
      <a href="home.php"><h2 class="secondary-title">Vá para a home</h2></a>
    <?php else:?>
      <h1>Algum erro ocorreu no cadastro :(</h1>
      <a href="index.php"><button>Voltar para a página de login/signin</button></a>
    <?php endif?>
  </body>
</html>