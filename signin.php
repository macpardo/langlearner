<?php
include_once 'banco.php';
?>

<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  </head>
  <body>
    <?php if ($_DB->add_usuario($_POST['username'], $_POST['password'])):?>
      <h1>Bem-vindo!</h1>
      <?php 
        echo "testing";
        session_start();
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['password'] = $_POST['password'];
        header("Location: home.php");
      ?>
      <a href="home.php"><button>Vá para a home</button></a>
    <?php else:?>
      <h1>Algum erro ocorreu no cadastro :(</h1>
      <a href="index.php"><button>Voltar para a página de login/signin</button></a>
    <?php endif?>
  </body>
</html>