<?php
session_start();
if (!isset($_SESSION['username'])) header('Location: index.php');
?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Lang Learner</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  </head>
  <body>
    <?php require 'topnav.php';?>
    <h1 class="main-title">LANG LEARNER</h1>
    <div class="content">
      <form action="insertpost.php" method="GET">
        <input type="text" name="title">
        <br>
        <textarea name="content" id="" cols="30" rows="10"></textarea>
        <br>
        <select name="lang" id="">
          <option value="en">English</option>
        </select>
        <input type="submit" value="Postar">
      </form>
    </div>
  </body>
</html>