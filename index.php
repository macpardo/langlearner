<html>
  <head>
    <title>Lang Learner</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  </head>
  <body>
    <h1 class="main-title">LANG LEARNER</h1>
    <div class="content">
      <h2>LOGIN</h2>
      <form action="login.php" method="POST">
        <input type="text" name="username">
        <br>
        <input type="password" name="password">
        <br>
        <input type="submit" value="Login">
      </form>
    </div>
    <div class="content">
      <h2>SIGN IN</h2>
      <form action="signin.php" method="POST">
        <input type="text" name="username">
        <br>
        <input type="password" name="password">
        <br>
        <input type="submit" value="Sign In">
      </form>
    </div>
  </body>
</html>