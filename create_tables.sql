CREATE TABLE usuario(
  username varchar(15) PRIMARY KEY NOT NULL,
  password varchar(50) NOT NULL
);

CREATE TABLE idioma(
  idioma varchar(10) PRIMARY KEY NOT NULL
);

CREATE TABLE post(
  postID int PRIMARY KEY NOT NULL,
  idioma varchar(10) NOT NULL,
  username varchar(15) NOT NULL,
  title varchar(100) NOT NULL,
  content varchar(10000) NOT NULL,
  upvotes int NOT NULL,
  downvotes int NOT NULL,
  FOREIGN KEY (username) REFERENCES usuario(username),
  FOREIGN KEY (idioma) REFERENCES idioma(idioma)
);

CREATE TABLE comentario(
  comentarioID int PRIMARY KEY NOT NULL,
  username varchar(15) NOT NULL,
  postID int NOT NULL,
  content varchar(300) NOT NULL,
  upvotes int NOT NULL,
  downvotes int NOT NULL,
  FOREIGN KEY (username) REFERENCES usuario(username),
  FOREIGN KEY (postID) REFERENCES post(postID)
);

CREATE TABLE palavra(
  palavra varchar(20) NOT NULL,
  idioma varchar(10) NOT NULL,
  PRIMARY KEY (palavra, idioma),
  FOREIGN KEY (idioma) REFERENCES idioma(idioma)
);

CREATE TABLE vocabulario(
  username varchar(15) NOT NULL,
  palavra varchar(20) NOT NULL,
  idioma varchar(10) NOT NULL,
  PRIMARY KEY (username, palavra, idioma),
  FOREIGN KEY (username) REFERENCES usuario(username),
  /*
  FOREIGN KEY (palavra) REFERENCES palavra(palavra),
  FOREIGN KEY (idioma) REFERENCES idioma(idioma)
  */
  FOREIGN KEY (palavra, idioma) REFERENCES palavra(palavra, idioma)
);