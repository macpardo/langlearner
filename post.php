<?php
require_once 'banco.php';
session_start();
if (!isset($_SESSION['username'])) header("Location: index.php");
$post = $_DB->get_post_from_id($_GET['postID'])->fetch_assoc();
$comentarios = $_DB->get_comments_from_post($_GET['postID']);
?>

<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  </head>
  <body>
    <?php require 'topnav.php';?>
    <h1 class="main-title">LANG LEARNER</h1>
    <div class="content">
      <h1 class="post-title"><?=$post['title']?></h1>
      <p>Autor: <?=$post['username']?></p>
      
      <!--<p><?=$post['content']?></p>-->
      <p>
        <?php foreach(preg_split('/(\s| |\.|,)+/', $post['content']) as $word):?>
          <a href="learnword.php?word=<?=$word?>&postID=<?=$post['postID']?>"><span class="
            <?php
              if($_DB->user_knows_word($_SESSION['username'], strtolower($word))) echo "knows"; else echo "notknows";
            ?>"><?=$word?></span></a>
        <?php endforeach?>
      </p>
      <p>
        <a href="upvotepost.php?postID=<?=$post['postId']?>"><i class="fa fa-arrow-up" aria-hidden="true"></i></a> <?=$post['upvotes']?> <a href="downvotepost.php?postID=<?=$post['postID']?>"><i class="fa fa-arrow-down" aria-hidden="true"></i></a> <?=$post['downvotes']?>
      </p>
      <form action="comentario.php" method="GET">
        <textarea name="comentario" cols="40" rows="5"></textarea>
        <br>
        <input type="hidden" name="postID" value="<?=$_GET['postID']?>">
        <input type="submit" value="Enviar comentario">
      </form>
      <?php while($comentario = $comentarios->fetch_assoc()):?>
        <hr>
        <h3 class="comment-title"><?=$comentario['username']?>:</h3>
        <p><?=$comentario['content']?></p>
        <p>
          <a href="upvotecomment.php?comentarioID=<?=$comentario['comentarioID']?>"><i class="fa fa-arrow-up" aria-hidden="true"></i></a> <?=$comentario['upvotes']?> <a href="downvotecomment.php?comentarioID=<?=$comentario['comentarioID']?>"><i class="fa fa-arrow-down" aria-hidden="true"></i></a> <?=$comentario['downvotes']?>
        </p>
      <?php endwhile?>
    </div>
  </body>
</html>