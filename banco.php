<?php
class Banco {
  private $host = "localhost", $usuario = "user", $senha = "sistema", $database = "langLearner";
  private $conexao;

  public function __construct() {
    $this->conexao = mysqli_connect($this->host, $this->usuario, $this->senha, $this->database);
  }
  public function __destruct() {
    mysqli_close($this->conexao);
  }

  public function add_usuario($username, $password) {
    $test = $this->conexao->query(
      "SELECT `username` FROM `usuario` WHERE `username` = '{$username}'"
    )->fetch_array(MYSQLI_NUM);
    if ($test != NULL) {
      return false;
    }
    else {
      return $this->conexao->query(
        "INSERT INTO `usuario`(`username`, `password`) VALUES ('{$username}', '{$password}')"
      );
    }
  }

  public function login($username, $password) {
    if ($this->conexao->query(
      "SELECT * FROM `usuario` WHERE `username` = '{$username}' AND `password` = '{$password}'"
    )->fetch_assoc()) {
        return true;
    }
    else {
      return false;
    }
  }

  public function usuario_existe($usuario) {
    if ($this->conexao->query(
      "SELECT `username` FROM `usuario` WHERE `usuario` = '{$username}'"
    )->fetch_assoc()) return true;
    else return false;
  }

  public function add_post($idioma, $username, $title, $content) {
    $postID = $this->conexao->query("SELECT MAX(postID) FROM post")->fetch_array(MYSQLI_NUM)[0];
    
    if ($postID == NULL) $postID = 0;
    else $postID++;

    $zero = 0;
    return $this->conexao->query(
      "INSERT INTO `post`(`postID`, `idioma`, `username`, `title`, `content`, `upvotes`, `downvotes`) 
      VALUES ('{$postID}', '{$idioma}', '{$username}', '{$title}', '{$content}', '{$zero}', '{$zero}')"
    );
  }

  public function upvote_post($postID) {
    return $this->conexao->query(
      "UPDATE `post` SET `upvotes` = `upvotes` + 1 WHERE `postID` = '{$postID}'"
    );
  }

  public function downvote_post($postID) {
    return $this->conexao->query(
      "UPDATE `post` SET `downvotes` = `downvotes` + 1 WHERE `postID` = '{$postID}'"
    );
  }

  public function add_comentario($username, $postID, $content) {
    $comentarioID = $this->conexao->query("SELECT MAX(comentarioID) FROM comentario")->fetch_array(MYSQLI_NUM)[0];

    if ($comentarioID == NULL) $comentarioID = 0;
    else $comentarioID++;

    $zero = 0;

    return $this->conexao->query(
      "INSERT INTO `comentario`(`comentarioID`, `username`, `postID`, `content`, `upvotes`, `downvotes`)
      VALUES ('{$comentarioID}', '{$username}', '{$postID}', '{$content}', '{$zero}', '{$zero}')"
    );
  }

  public function upvote_comentario($comentarioID) {
    return $this->conexao->query(
      "UPDATE `comentario` SET `upvotes` = `upvotes` + 1 WHERE `comentarioID` = '{$comentarioID}'"
    );
  }

  public function downvote_comentario($comentarioID) {
    return $this->conexao->query(
      "UPDATE `comentario` SET `downvotes` = `downvotes` + 1 WHERE `comentarioID` = '{$comentarioID}'"
    );
  }

  public function add_palavra($palavra, $idioma) {
    $idioma_existe = $this->conexao->query(
      "SELECT `idioma` FROM `idioma` WHERE `idioma` = '{$idioma}'"
    )->fetch_array(MYSQLI_NUM);
    
    if ($idioma_existe == NULL) return false;

    $test = $this->conexao->query(
      "SELECT * FROM `palavra` WHERE `palavra` = '{$palavra}' AND `idioma` = '{$idioma}'"
    )->fetch_array(MYSQLI_NUM);

    if ($test != NULL) {
      return false;
    }
    else {
      return $this->conexao->query(
        "INSERT INTO `palavra`(`palavra`, `idioma`)
        VALUES ('{$palavra}', '{$idioma}')"
      );
    }
  }

  public function add_vocabulario($username, $palavra, $idioma) {
    return $this->conexao->query(
      "INSERT INTO `vocabulario`(`username`, `palavra`, `idioma`)
      VALUES ('{$username}', '{$palavra}', '{$idioma}')"
    );
  }

  public function get_posts_from_language($language) {
    return $this->conexao->query(
      "SELECT * FROM `post` WHERE `idioma` = '{$language}'"
    );
  }

  public function get_posts_from_user($username) {
    return $this->conexao->query(
      "SELECT * FROM `post` WHERE `username` = '{$username}'"
    );
  }

  public function get_post_from_id($postID) {
    return $this->conexao->query(
      "SELECT * FROM `post` WHERE `postID` = '{$postID}'"
    );
  }

  public function get_comments_from_post($postID) {
    return $this->conexao->query(
      "SELECT * FROM `comentario` WHERE `postID` = '{$postID}'"
    ) ;
  }

  public function user_knows_word($username, $palavra) {
    if ($this->conexao->query(
      "SELECT * FROM `vocabulario` WHERE `username` = '{$username}' AND `palavra` = '{$palavra}'"
    )->fetch_assoc() == NULL) {
      return false;
    }
    else {
      return true;
    }
  }

  public function get_difficulty($username, $postID) {
    $post = $this->conexao->query("SELECT * FROM `post` WHERE `postID` = '{$postID}'")->fetch_assoc();
    $text = strtolower($post['content']);
    $language = $post['idioma'];
    $words = preg_split("/(\s|\.|,| |\?|!)+/", $text);
    $existing_words = 0;
    $known_words = 0;
    foreach ($words as $word) {
      $word_exists = $this->conexao->query(
        "SELECT * FROM `palavra` WHERE `palavra` = '{$word}' AND `idioma` = '{$language}'"
      )->fetch_assoc();
      $user_knows = $this->conexao->query(
        "SELECT * FROM `vocabulario` WHERE `username` = '{$username}' AND `palavra` = '{$word}' AND `idioma` = '{$language}'"
      )->fetch_assoc();
      if ($word_exists) $existing_words++;
      if ($user_knows) $known_words++;
    }
    return (1.0 - $known_words / $existing_words) * 100;
  }

  public function get_postID_from_comentarioID($comentarioID) {
    return $this->conexao->query(
      "SELECT `postID` FROM `comentario` WHERE `comentarioID` = '{$comentarioID}'"
    )->fetch_assoc()['postID'];
  }
}

$_DB = new Banco();
?>