# LangLearner #

Este foi o trabalho final da matéria de Programação II, no segundo semestre de 2017.

### Qual é o prósito do LangLearner? ###

* Aprender inglês através da leitura de textos

### Como funciona? ###

* Os usuários fazem posts em inglês
* Cada usuário tem seu vocabulário registrado no banco de dados
* O usuário pode ver a porcentagem de palavras desconhecidas antes de visualizar o post
* Lendo o post, as palavras desconhecidas ficam com o fundo cinza
* Elas podem ser adicionadas ao vocabulário com um click