<?php
require_once 'banco.php';
session_start();
$posts = $_DB->get_posts_from_language('en');
if (!isset($_SESSION["username"])) header("Location: index.php");
?>

<html>
  <head>
    <meta charset="utf-8">
    <title>Lang Learner</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  </head>
  <body>
    <?php require 'topnav.php';?>
    <h1 class="main-title">LANG LEARNER</h1>
    <div class="content">
      <?php while($post = $posts->fetch_assoc()):?>
        <a href="post.php?postID=<?=$post['postID']?>">
          <div class="post" id="<?=$post['postID']?>">
            <h2 class="post-title"><?=$post['title']?></h2>
            <p>Autor: <?=$post['username']?></p>
            <p>Dificuldade: <?=intval($_DB->get_difficulty($_SESSION['username'], $post['postID']))?></p>
            <p><i class="fa fa-arrow-up" aria-hidden="true"></i> <?=$post['upvotes']?> <i class="fa fa-arrow-down" aria-hidden="true"></i> <?=$post['downvotes']?></p>
          </div>
          <hr>
        </a>
      <?php endwhile?>
    </div>
  </body>
</html>